
// var elm3 = document.querySelector('#animate1');

// var onScroll = (function(){
//   var startPos;
  
//   function run(){
//     var fromTop = window.pageYOffset, 
//         rect = elm3.getBoundingClientRect(),
//         scrollDelta;

//     // check if element is in viewport
//     if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
//        startPos = startPos === undefined ? fromTop : startPos;
//     else{
//       startPos = 0;
//       return;
//     }
    
//     scrollDelta = (fromTop) * 1; // "speed" per scrolled frame
//     elm3.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * 0.0012}, 1)`;
 
//     console.clear();
//     console.log(scrollDelta);
//   }
  
//   run();
  
//   return run;
// })()
// window.addEventListener('scroll', onScroll);



var elm = document.querySelector('#animate2');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elm.getBoundingClientRect(),
        scrollDelta;
    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elm.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * 0.00101}, 1)`;
    elm.style.width = `300px`
    elm.style.position = `relative`;
    elm.style.left = `900px`
    elm.style.top = `-30px`

    console.clear();
    console.log(scrollDelta);
  }
  run(); 
  return run;
})()
window.addEventListener('scroll', onScroll);



var elm11 = document.querySelector('.right-to-left-animatable');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elm11.getBoundingClientRect(),
        scrollDelta;

    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elm11.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * -0.0012}, 1)`;
    elm11.style.width = `150px`;
    elm11.style.position = `relative`;
    elm11.style.left = `900px`
    console.clear();
    console.log(scrollDelta);
  }
  run();
  return run;
})()
window.addEventListener('scroll', onScroll);


var elm13 = document.querySelector('#animate3');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elm13.getBoundingClientRect(),
        scrollDelta;

    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elm13.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * -0.0012}, 1)`;
    elm13.style.width = `200px`;
    elm13.style.position = `relative`;
    elm13.style.left = `0px`
    console.clear();
    console.log(scrollDelta);
  }
  run();
  return run;
})()
window.addEventListener('scroll', onScroll);

var elm37 = document.querySelector('#animate4');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elm37.getBoundingClientRect(),
        scrollDelta;

    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elm37.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * 0.0012}, 1)`;
    elm37.style.width = `200px`;
    elm37.style.position = `relative`;
    elm37.style.left = `0px`
    console.clear();
    console.log(scrollDelta);
  }
  run();
  return run;
})()
window.addEventListener('scroll', onScroll);

var elm4 = document.querySelector('#animate6');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elm4.getBoundingClientRect(),
        scrollDelta;

    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elm4.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * 0.0012}, 1)`;
    elm4.style.width = `200px`;
    elm4.style.position = `relative`;
    elm4.style.left = `900px`
    console.clear();
    console.log(scrollDelta);
  }
  run();
  return run;
})()
window.addEventListener('scroll', onScroll);

var elm5 = document.querySelector('#animate7');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elm5.getBoundingClientRect(),
        scrollDelta;

    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elm5.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * -0.0012}, 1)`;
    elm5.style.width = `100px`;
    elm5.style.position = `relative`;
    elm5.style.left = `1050px`
    console.clear();
    console.log(scrollDelta);
  }
  run();
  return run;
})()
window.addEventListener('scroll', onScroll);

var elm8 = document.querySelector('#animate8');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elm8.getBoundingClientRect(),
        scrollDelta;

    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elm8.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * 0.0012}, 1)`;
    elm8.style.width = `150px`;
    elm8.style.position = `relative`;
    elm8.style.left = `0px`
    console.clear();
    console.log(scrollDelta);
  }
  run();
  return run;
})()
window.addEventListener('scroll', onScroll);

var elmn = document.querySelector('#animate9');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elmn.getBoundingClientRect(),
        scrollDelta;

    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elmn.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * -0.00123}, 1)`;
    elmn.style.width = `100px`;
    elmn.style.position = `relative`;
    elmn.style.left = `0px`
    console.clear();
    console.log(scrollDelta);
  }
  run();
  return run;
})()
window.addEventListener('scroll', onScroll);

var elmnl = document.querySelector('#animate10');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elmnl.getBoundingClientRect(),
        scrollDelta;

    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elmnl.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * -0.00123}, 1)`;
    elmnl.style.width = `150px`;
    elmnl.style.position = `relative`;
    elmnl.style.left = `970px`
    console.clear();
    console.log(scrollDelta);
  }
  run();
  return run;
})()
window.addEventListener('scroll', onScroll);

var elmni = document.querySelector('#animate11');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elmni.getBoundingClientRect(),
        scrollDelta;

    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop - startPos) * 1; // "speed" per scrolled frame
    elmni.style.transform = `translate3d(0px, 0px, 0px) scale(${scrollDelta * -0.00123}, 1)`;
    elmni.style.width = `80px`;
    elmni.style.position = `relative`;
    elmni.style.left = `1150px`
    console.clear();
    console.log(scrollDelta);
  }
  run();
  return run;
})()
window.addEventListener('scroll', onScroll);

var elm12 = document.querySelector('.top');
var onScroll = (function(){
  var startPos;
  function run(){
    var fromTop = window.pageYOffset, 
        rect = elm12.getBoundingClientRect(),
        scrollDelta;
    // check if element is in viewport
    if( (rect.top - window.innerHeight) <= 0 && rect.bottom > 0 )
       startPos = startPos === undefined ? fromTop : startPos;
    else{
      startPos = 0;
      return;
    }
    scrollDelta = (fromTop -  startPos ) * 1; // "speed" per scrolled frame
    // elm12.style.position = `relative`;
    // elm12.style.top = `-30px`
    elm12.style.opacity = `(${scrollDelta-1})`;
    console.clear();
    console.log(scrollDelta);
  }
run();
return run;
})()
window.addEventListener('scroll', onScroll);




var lastScrollTop = 0;
$("html").scroll(function (event) {
    var st = $(this).scrollTop();
    if (st > lastScrollTop) {
        $('.top').animate({top: '-=10'}, 0);
    } else {
        $('.top').animate({top: '+=10'}, 0);
    }
    lastScrollTop = st;
});

var $sun1 = $('#photo-animate3');
var $win1 = $(window);
$win1.on('scroll', function () {
  var top = $win1.scrollTop() / 15;
  $sun1.css('transform', `rotate(${--top}deg)`);
});

var $sun2 = $('#photo-animate5');
var $win2 = $(window);
$win2.on('scroll', function () {
  var top = $win2.scrollTop() / 15;
  $sun2.css('transform', `rotate(${--top}deg)`);
});

var $sun3 = $('#photo-animate6');
var $win3 = $(window);
$win3.on('scroll', function () {
  var top = $win3.scrollTop() / 10;
  $sun3.css('transform', `rotate(${--top}deg)`);
});

